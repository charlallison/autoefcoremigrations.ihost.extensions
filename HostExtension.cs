﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AutoEFCoreMigration.IHost.Extensions {
    public static class HostExtension {

        public static Microsoft.Extensions.Hosting.IHost RunMigration<T>(this Microsoft.Extensions.Hosting.IHost webHost) where T : DbContext {
            using var scope = webHost.Services.CreateScope();
            var services = scope.ServiceProvider;

            try {
                var dbService = services.GetRequiredService<T>();
                var migrationCount = dbService.Database.GetPendingMigrations().Count();
                Console.WriteLine($"{migrationCount} pending migrations found.");

                if (migrationCount > 0) {
                    Console.WriteLine("Database migration started...");
                    dbService.Database.Migrate();
                    Console.WriteLine("Database migration completed.");
                }
                
            }catch(Exception ex) {
                Console.Error.WriteLine("Error in migrating database.");
                Console.Error.WriteLine(ex.StackTrace);
            }

            return webHost;
        }

    }
}